//
//  AppDelegate.swift
//  Source Control
//
//  Created by Ommar Alvarez on 5/26/19.
//  Copyright © 2019 Ommar Alvarez. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

